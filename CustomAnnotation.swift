//
//  CustomAnnotation.swift
//  LagunasDeRuidera
//
//  Created by ginppian on 31/08/16.
//  Copyright © 2016 ginppian. All rights reserved.
//

import Foundation
import MapKit

class CustomAnnotation: MKPointAnnotation {
    var customPin: String!
    var miniatureImg: String!
    var id: String!
    var audio: String!
    var des: String!
}
