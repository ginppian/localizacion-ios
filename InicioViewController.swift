//
//  InicioViewController.swift
//  LagunasDeRuidera
//
//  Created by ginppian on 26/08/16.
//  Copyright © 2016 ginppian. All rights reserved.
//

import UIKit
import AVFoundation

class InicioViewController: UIViewController {
    //🇺🇸🇲🇽🇪🇸
    let defaults = UserDefaults.standard
    //0 Espanol
    //1 Ingles
    //key = idioma
    //🇺🇸🇲🇽🇪🇸
    
    //Referencia ImageView
    @IBOutlet weak var espanol: UIImageView!
    @IBOutlet weak var ingles: UIImageView!
    
    //Referencia Botones
    @IBOutlet weak var espanolButton: UIButton!
    @IBOutlet weak var inglesButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let bandera: Int? = UserDefaults.standard.integer(forKey: "idioma")
        if (bandera! != nil) {
            
            // nos vamos directamente a la pantalla de mapa
            //let storyboard = UIStoryboard(name: "Main", bundle: nil)
            //let controller = storyboard.instantiateViewController(withIdentifier: "Mapa_ViewController") as! UIViewController
            //self.present(controller, animated: true, completion: nil)
        }
        
        super.viewDidAppear(true)
    }
    
    
    //🇺🇸🇲🇽🇪🇸
    @IBAction func espanolButtonAction(sender: UIButton) {
        self.defaults.set(0, forKey: "idioma")
        performSegue(withIdentifier: "Bienvenido_Segue", sender: self)
    
    }
    //🇺🇸🇲🇽🇪🇸
    //🇺🇸🇲🇽🇪🇸
    @IBAction func inglesButtonAction(sender: UIButton) {
        self.defaults.set(1, forKey: "idioma")
        performSegue(withIdentifier: "Bienvenido_Segue", sender: self)
        
    }
    //🇺🇸🇲🇽🇪🇸
    //Pasa al otro StoryBoard
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Bienvenido_Segue"{
            
        }
        
    }

}
