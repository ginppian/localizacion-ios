//
//  PuntoInteres.swift
//  LagunasDeRuidera
//
//  Created by ginppian on 02/09/16.
//  Copyright © 2016 ginppian. All rights reserved.
//

import UIKit
import CoreLocation

class Geotification: NSObject {
    
    var id: String! = String()
    var name: String! = String()
    var subname: String! = String()
    var des: String! = String()
    var lat: CLLocationDegrees! = CLLocationDegrees()
    var lng: CLLocationDegrees! = CLLocationDegrees()
    var distance: Double = Double()
    var route: String! = String()
    var icon: String! = String()
    var audio: String! = String()
    var img: String! = String()
    var radio: Double = Double()
        
}
