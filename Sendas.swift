//
//  Sendas.swift
//  LagunasDeRuidera
//
//  Created by ginppian on 03/09/16.
//  Copyright © 2016 ginppian. All rights reserved.
//

import UIKit

class Sendas: NSObject {
    var id: Int = Int()
    var idtext: String = String()
    var name: String = String()
    var des: String = String()
    var duration: String = String()
    var longitude: String = String()
    var img: String = String()
    var numPoints: String = String()
    var points: String = String()
    var audio: String = String()
}
