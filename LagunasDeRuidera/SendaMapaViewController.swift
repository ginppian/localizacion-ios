//
//  SendaMapaViewController.swift
//  LagunasDeRuidera
//
//  Created by David Santiago Fdez-Bermejo Morales on 4/2/17.
//  Copyright © 2017 ginppian. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class SendaMapaViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    
    
    let locationManager = CLLocationManager()
    let authorizationStatus = CLLocationManager.authorizationStatus()
    var myCurrentLocation_ = CLLocation()
    
    let defaults = UserDefaults.standard
    var bandera = Int()
    var idiomaArchivo = String()
    
    //Punto de incio
    let latitud:CLLocationDegrees = 38.951437
    let longitud:CLLocationDegrees = -2.860949
    //Campo de vision
    let latDelta:CLLocationDegrees = 0.05
    let longDelta:CLLocationDegrees = 0.05
    
    var sendaInfo = Sendas()
    
    @IBOutlet weak var mapa: MKMapView!
    
    
    @IBOutlet weak var changeMapType: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //🇺🇸🇲🇽🇪🇸 - Escojemos el idioma
        self.bandera = self.defaults.integer(forKey: "idioma")
        if(self.bandera == 0){
            self.idiomaArchivo = "routes"
        }
        else if (self.bandera == 1) {
            self.idiomaArchivo = "routes_en"
        } else {
            print("Error al asignar la bandera de idioma")
        }
        //🇺🇸🇲🇽🇪🇸 - Escojemos el idioma
        
        //✈️ MAPA
        mapa.delegate = self
        //✈️ MAPA
        
        // centramos MAPA
        let span: MKCoordinateSpan = MKCoordinateSpanMake(latDelta, longDelta)
        let location: CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitud, longitud)
        
        //Construimos la region del punto inicial y la vista span
        let region:MKCoordinateRegion = MKCoordinateRegionMake(location, span)
        //Agregamos al mapa
        mapa.setRegion(region, animated: true)
        
        //Agregamos el logo al Navigation Bar
        let topNB = UIImage(named: "cabecera_mapa_azul.png")
        let topNBR = topNB?.resizeImageNav(newWidth: 250)
        let topNBView = UIImageView(image: topNBR)
        self.navigationController?.navigationBar.topItem?.titleView = topNBView
        self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.navigationBar.isTranslucent = false
        
        //📌🙋🚂 - Encontramos nuestra ubicacion
        //solicitamos al usuario autorizacion para usar el GPS
        if(authorizationStatus == .authorizedWhenInUse || authorizationStatus == .authorizedAlways) {
            locationManager.startUpdatingLocation()
        }
        else
        {
            locationManager.requestAlwaysAuthorization()
        }
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.activityType = CLActivityType.fitness
        locationManager.distanceFilter = 100
        locationManager.startMonitoringSignificantLocationChanges()
        
        
        
        let points = JSONReader.readRoutePointsToArray(fileName: self.idiomaArchivo, id: sendaInfo.idtext)
        
        var coordinates = [CLLocationCoordinate2D]()
        
        for point in points[0] {
            coordinates.append(CLLocationCoordinate2D(latitude: point[0], longitude: point[1]))
        }
        
        let polyline = MKPolyline(coordinates: &coordinates, count: coordinates.count)
        
        // añadimos la linea al mapa
        self.mapa.add(polyline, level: MKOverlayLevel.aboveRoads)
        
        
        
        
        
        
        // centramos mapa en el punto de inicio de la ruta
        let center = CLLocationCoordinate2DMake(points[0][0][0], points[0][0][1])
        
        let centerRegion = MKCoordinateRegionMake(center, span)
        
        //Agregamos al mapa la posición
        self.mapa.setRegion(centerRegion, animated: true)
        
    }
    
    
    //📌🙋🚂 - Seguir al usuario en el mapa - tracking (punto azul)
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print("Entra a: didChangeAuthorization")
        mapa.showsUserLocation = (status == .authorizedAlways)
        
    }
    //📌🙋🚂 - Seguir al usuario en el mapa - tracking (punto azul)
    
    //📌🙋🚂 - Actualiza
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("didUpdate")
        
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        let region = MKCoordinateRegion(center: locValue, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        print(locationManager.monitoredRegions)
        
        //Guardamos nuestra curren location
        var array = locations
        myCurrentLocation_ = array.popLast()!
        
        //Actualizamos el mapa en donde nos dirigimos
        //self.mapa.setRegion(region, animated: true)
        
    }
    //📌🙋🚂 - Actualiza
    
    //📌🙋🚂 - Error en el perimetro
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        print("Monitoring failed for region with identifier: \(region!.identifier)")
        
    }
    //📌🙋🚂 - Error en el perimetro
    
    //📌🙋🚂 - Manejo de errorers
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location Manager failed with the following error: \(error)")
        
    }
    //📌🙋🚂 - Manejo de errorers
    
    //📌🙋🚂 - Como en background nos desja checar hasta 20 lugares, obtenemos los mas cercanos
    func getNearPlaceTo(myCurrentLocation: CLLocation, geofences:[Geotification]){
        var sortedGeo = [Geotification]()
        
        if geofences.count > 0 {
            for geofence in geofences{
                let location = CLLocation(latitude: geofence.lat, longitude: geofence.lng)
                let distance = myCurrentLocation.distance(from: location)
                geofence.distance = distance
                sortedGeo.append(geofence)
                
            }
            //Ordenamos los objetos por su propiedad .distance
            sortedGeo = sortedGeo.sorted(by: {$0.distance < $1.distance})
            //Limpiamos overlays antiguos
            self.mapa.removeOverlays(self.mapa.overlays)
            //Agregamos overlay al lugar(s) mas cercano(s)
            
            addRadiusOverlay(forGeotification: sortedGeo[0])
            addRadiusOverlay(forGeotification: sortedGeo[1])
            
            self.startMonitoring(geotification: sortedGeo[0])
            self.startMonitoring(geotification: sortedGeo[1])
            
        }
        
    }
    //📌🙋🚂 - Como en background nos desja checar hasta 20 lugares, obtenemos los mas cercanos
    
    //📌🙋🚂 - Agregamos un radio visual al marcador
    func addRadiusOverlay(forGeotification geotification: Geotification) {
        let lat = geotification.lat
        let lng = geotification.lng
        let coordinate = CLLocationCoordinate2D(latitude: lat!, longitude: lng!)
        let radius = CLLocationDistance(Double(geotification.radio))
        
        mapa?.add(MKCircle(center: coordinate, radius: radius))
        
    }
    //📌🙋🚂 - Agregamos un radio al marcador
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        if overlay is MKCircle {
            let circleRenderer = MKCircleRenderer(overlay: overlay)
            circleRenderer.lineWidth = 1.0
            circleRenderer.strokeColor = .purple
            circleRenderer.fillColor = UIColor.purple.withAlphaComponent(0.4)
            return circleRenderer
        }
        
        if overlay is MKPolyline {
            // draw the track
            let renderer = MKPolylineRenderer(overlay: overlay)
            renderer.strokeColor = UIColor.orange
            renderer.lineWidth = 3
            return renderer
        }
        
        return MKOverlayRenderer(overlay: overlay)
        
    }
    //📌🙋🚂 - Agregamos un radio al marcador
    
    //📌🙋🚂 - Agregamos un radio logico al marcador
    func region(withGeotification geotification: Geotification) -> CLCircularRegion {
        let coordinate = CLLocationCoordinate2D(latitude: geotification.lat, longitude: geotification.lng)
        let radius = CLLocationDistance(geotification.radio)
        // 1
        let region = CLCircularRegion(center: coordinate, radius: radius, identifier: geotification.id)
        // 2
        region.notifyOnEntry = true
        
        return region
    }
    //📌🙋🚂 - Agregamos un radio al marcador
    
    //📌🙋🚂 - Agregamos un radio al marcador
    func startMonitoring(geotification: Geotification) {
        // 1
        if !CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
            print("Geofencing is not supported on this device!")
            return
        }
        // 2
        if CLLocationManager.authorizationStatus() != .authorizedAlways {
            print("Your geotification is saved but will only be activated once you grant Geotify permission to access the device location.")
        }
        // 3
        let region = self.region(withGeotification: geotification)
        // 4
        locationManager.startMonitoring(for: region)
        
        print(locationManager.monitoredRegions)
    }
    //📌🙋🚂 - Agregamos un radio al marcador
    
    
    
    @IBAction func changeMap(_ sender: Any) {
        if self.mapa.mapType == MKMapType.standard {
            self.mapa.mapType = MKMapType.satellite
        } else {
            self.mapa.mapType = MKMapType.standard
        }
    }
    
    
}
