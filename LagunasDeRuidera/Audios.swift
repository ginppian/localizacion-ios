//
//  Audios.swift
//  LagunasDeRuidera
//
//  Created by ginppian on 03/09/16.
//  Copyright © 2016 ginppian. All rights reserved.
//

import UIKit

class Audios: NSObject {
    var id: Int   = Int()
    var name: String = String()
    var file: String = String()
    var text: String = String()
    var audio: String = String()
}
