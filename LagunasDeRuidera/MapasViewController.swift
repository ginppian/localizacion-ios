//
//  MapasViewController.swift
//  LagunasDeRuidera
//
//  Created by ginppian on 27/08/16.
//  Copyright © 2016 ginppian. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

extension CGRect{
    init(_ x:CGFloat,_ y:CGFloat,_ width:CGFloat,_ height:CGFloat) {
        self.init(x:x,y:y,width:width,height:height)
    }
    
}
extension CGSize{
    init(_ width:CGFloat,_ height:CGFloat) {
        self.init(width:width,height:height)
    }
    
}
extension CGPoint{
    init(_ x:CGFloat,_ y:CGFloat) {
        self.init(x:x,y:y)
    }
    
}
extension MKMapView {
    func zoomToUserLocation() {
        guard let coordinate = self.userLocation.location?.coordinate else { return }
        let region = MKCoordinateRegionMakeWithDistance(coordinate, 2000, 2000)
        setRegion(region, animated: true)
    }
    
}
extension UIImage{
    //Cambia el tamano de la imagen para agregarla al globito
    func resizeImage(newWidth: CGFloat) -> UIImage
    {
        let scale = newWidth / self.size.width
        let newHeight = self.size.height * scale
        UIGraphicsBeginImageContext(CGSize(newWidth, newHeight*1.5))
        self.draw(in: CGRect(0, 0, newWidth, newHeight*1.5))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
        
    }
    //La misma solo que aqui escalamos proporcionalmente para la barra de navegacion
    func resizeImageNav(newWidth: CGFloat) -> UIImage
    {
        let scale = newWidth / self.size.width
        let newHeight = self.size.height * scale
        UIGraphicsBeginImageContext(CGSize(newWidth, newHeight))
        self.draw(in: CGRect(0, 0, newWidth, newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
        
    }
    
}
class MapasViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    //📌🙋🚂
    let locationManager = CLLocationManager()
    let authorizationStatus = CLLocationManager.authorizationStatus()
    var myCurrentLocation_ = CLLocation()
    //📌🙋🚂
    
    // Botones, para cambiarle el title
    @IBOutlet weak var boton1: UIButton!
    @IBOutlet weak var boton2: UIButton!
    @IBOutlet weak var boton3: UIButton!
    
    
    // botón para cambiar el tipo de mapa
    @IBOutlet weak var ChangeMapType: UIButton!
    
    //✈️ MAPA
    @IBOutlet weak var mapa: MKMapView!
    //✈️ MAPA
    
    //Punto de incio
    let latitud:CLLocationDegrees = 38.951437
    let longitud:CLLocationDegrees = -2.860949
    //Campo de vision
    let latDelta:CLLocationDegrees = 0.05
    let longDelta:CLLocationDegrees = 0.05
    
    var arrayLat = [String]()
    var arrayLng = [String]()
    var arrayNme = [String]()
    var arrayDesc = [String]()
    var arraySub = [String]()
    var arrayImg = [String]()
    var arrayID  = [String]()
    var arrayIcon = [String]()
    var arrayAud = [String]()
    var arrayRadio = [String]()
    var arrayGeo = [Geotification]()
    
    //Geotification
    var geotifi = Geotification()
    
    //🇺🇸🇲🇽🇪🇸
    let defaults = UserDefaults.standard
    var bandera = Int()
    var idiomaArchivo = String()
    //espanol = 0
    //defaults.integerForKey("espanol")
    //ingles = 1
    //defaults.integerForKey("ingles")
    //🇺🇸🇲🇽🇪🇸
    
    //ℹ️
    //Pagina Web
    var botonSeleccionado = String()
    
    //👷🏽👷🏽👷🏽👷🏽👷🏽👷🏽👷🏽 - Constructor
    override func viewDidLoad() {
        super.viewDidLoad()
    
        //🇺🇸🇲🇽🇪🇸 - Escojemos el idioma
        self.bandera = self.defaults.integer(forKey: "idioma")
        if(self.bandera == 0){
            self.idiomaArchivo = "locations"
        }
        else if (self.bandera == 1) {
            self.idiomaArchivo = "locations_en"
            
            // le cambiamos los títulos a los botones
            self.boton1.setTitle("Natural Park", for: .normal)
            self.boton2.setTitle("Information", for: .normal)
            self.boton3.setTitle("Don Quixote", for: .normal)
            
        } else {
            print("Error al asignar la bandera de idioma")
        }
        //🇺🇸🇲🇽🇪🇸 - Escojemos el idioma
        
        //🇺🇸🇲🇽🇪🇸 - Llenamos los arreglos
        arrayLat = JSONReader.readJSONToArrayString(fileName: idiomaArchivo, type: ".json", key: "lat")
        arrayLng = JSONReader.readJSONToArrayString(fileName: idiomaArchivo, type: ".json", key: "lng")
        arrayNme = JSONReader.readJSONToArrayString(fileName: idiomaArchivo, type: ".json", key: "name")
        arrayDesc = JSONReader.readJSONToArrayString(fileName: idiomaArchivo, type: ".json", key: "description")
        arraySub = JSONReader.readJSONToArrayString(fileName: idiomaArchivo, type: ".json", key: "subname")
        arrayImg = JSONReader.readJSONToArrayString(fileName: idiomaArchivo, type: ".json", key: "img")
        arrayID = JSONReader.readJSONToArrayString(fileName: idiomaArchivo, type: ".json", key: "id")
        arrayIcon = JSONReader.readJSONToArrayString(fileName: idiomaArchivo, type: ".json", key: "icon")
        arrayAud = JSONReader.readJSONToArrayString(fileName: idiomaArchivo, type: ".json", key: "audio")
        arrayRadio = JSONReader.readJSONToArrayString(fileName: idiomaArchivo, type: ".json", key: "distance")
        
        for i in 0 ..< arrayLat.count {
            let geofencing = Geotification()
                geofencing.lat = CLLocationDegrees(Double(arrayLat[i])!)
                geofencing.lng = CLLocationDegrees(Double(arrayLng[i])!)
                geofencing.name = arrayNme[i]
                geofencing.des = arrayDesc[i]
                geofencing.subname = arraySub[i]
                geofencing.img = arrayImg[i]
                geofencing.id = arrayID[i]
                geofencing.icon = arrayIcon[i]
                geofencing.audio = arrayAud[i]
                //⚠️ importante que las llaves y los elementos todo vaya entre comillas pues el lector de JSON todo lo regresa como cadena
                geofencing.radio = Double(arrayRadio[i])!
            
                arrayGeo.append(geofencing)
            
        }
        //🇺🇸🇲🇽🇪🇸 - Llenamos los arreglos
        
        //✈️ MAPA
        mapa.delegate = self
        //✈️ MAPA
        
        // centramos MAPA
        let span: MKCoordinateSpan = MKCoordinateSpanMake(latDelta, longDelta)
        let location: CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitud, longitud)
        //Construimos la region del punto inicial y la vista span
        let region:MKCoordinateRegion = MKCoordinateRegionMake(location, span)
        //Agregamos al mapa
        mapa.setRegion(region, animated: true)
        
        //✈️ MAPA - Agregamos los marcadores
        for i in 0 ..< arrayLat.count {
            
            let anotation = CustomAnnotation()

            anotation.coordinate = CLLocationCoordinate2DMake(Double(arrayLat[i])!, Double(arrayLng[i])!)
            anotation.title = arrayNme[i]
            anotation.subtitle = arraySub[i]
            anotation.des = arrayDesc[i]
            anotation.customPin = arrayIcon[i] + ".png"
            anotation.miniatureImg = arrayImg[i]
            anotation.id = arrayID[i]
            anotation.audio = arrayAud[i]
            
            // esto es un poco chapuza, debería de coger del fichero audio audio.json
            if(self.bandera == 1) {
                anotation.audio = arrayAud[i] + "_en"
            }
            
            mapa.addAnnotation(anotation)
        }
        //✈️ MAPA - Agregamos los marcadores
        
        //Agregamos el logo al Navigation Bar
        let topNB = UIImage(named: "cabecera_mapa_azul.png")
        let topNBR = topNB?.resizeImageNav(newWidth: 250)
        let topNBView = UIImageView(image: topNBR)
        self.navigationController?.navigationBar.topItem?.titleView = topNBView
        self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.navigationBar.isTranslucent = false

        //📌🙋🚂 - Encontramos nuestra ubicacion
        //solicitamos al usuario autorizacion para usar el GPS
        if(authorizationStatus == .authorizedWhenInUse || authorizationStatus == .authorizedAlways) {
            locationManager.startUpdatingLocation()
        }
        else
        {
            locationManager.requestAlwaysAuthorization()
        }
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.activityType = CLActivityType.fitness
        locationManager.distanceFilter = 100
        locationManager.startMonitoringSignificantLocationChanges()
        
        /*NOTA: .startMonitoringSignificantLocationChanges y .distanceFilter trabajan en conjunto:
         1.- La primera da la orden de entrar a la funcion didUpdateLocations siempre que el usuario haya transcurrido un tramo significativo de metros.
         2.- La segunda es la distancia o el tramo en metros que debe recorrer el usuario para activar la primera.
         */
        //📌🙋🚂 - Encontramos nuestra ubicacion

    }
    //👷🏽👷🏽👷🏽👷🏽👷🏽👷🏽👷🏽 - Constructor
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        // miramos si tenemos que pintar una ruta
        
    }

    
    //✈️ MAPA - Despliega los marcadores
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if !(annotation is CustomAnnotation) {
            return nil
        }
        
        let reuseId = "test"
        
        var anView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            anView!.canShowCallout = true
        }
        else {
            anView!.annotation = annotation
        }
        
        let cpa = annotation as! CustomAnnotation
        anView!.image = UIImage(named:cpa.customPin)
        
        anView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        
        //Las imagenes deben tener extencion JPG
        var imgR = UIImage()
        var namFull = cpa.miniatureImg + ".jpg"
        let img: UIImage? = UIImage(named: namFull)
        
        //Se pone esto porque algunas imagenes falta
        if img != nil{
            imgR = (img?.resizeImage(newWidth: 55))!
        } else {
            namFull = "barreralacolgada.jpg"
            let auxImag = UIImage(named: namFull)!
            imgR = auxImag.resizeImage(newWidth: 55)
        }
        
        anView?.leftCalloutAccessoryView = UIImageView(image: imgR)

        //Guardamos la info que vamos a pasar a la siguiente vista
        
        return anView
    
    }
    //✈️ MAPA - Despliega los marcadores

    //✈️ MAPA - Agrega una accion a los marcadores
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if control == view.rightCalloutAccessoryView {
            
            var selectedAnnotation: CustomAnnotation!
            
            selectedAnnotation = view.annotation as! CustomAnnotation
            
            //📍Gaurdamos los datos enviar a la Vista Detallada
            if selectedAnnotation.miniatureImg != nil{
                geotifi.img = selectedAnnotation.miniatureImg + ".jpg"
                
            } else {
                geotifi.img = "barreralacolgada.jpg"

            }
            geotifi.id = selectedAnnotation.id!
            geotifi.name = selectedAnnotation.title!
            geotifi.subname = selectedAnnotation.subtitle!
            geotifi.audio = selectedAnnotation.audio!
            geotifi.des = selectedAnnotation.des!
            //📍Gaurdamos los datos enviar a la Vista Detallada

            performSegue(withIdentifier: "Mapa_Segue_InfoMapa", sender: self)
        }
        
    }
    //✈️ MAPA - Agrega una accion a los marcadores

    //ℹ️ - Informacion
    @IBAction func butonInformacionPageWeb(sender: UIButton) {
        botonSeleccionado = "informacion"
        performSegue(withIdentifier: "Mapa_PaginaWeb_Segue", sender: self)
        
    }
    //ℹ️ - Informacion
    
    //ℹ️ - Normatividad
    @IBAction func butonNormativaPageWeb(sender: UIButton) {
        botonSeleccionado = "normativa"
        performSegue(withIdentifier: "Mapa_PaginaWeb_Segue", sender: self)
        
    }
    //ℹ️ - Normatividad

    //ℹ️ - Que hacer
    @IBAction func butonQueHacerPagWeb(sender: UIButton) {
        botonSeleccionado = "queHacer"
        performSegue(withIdentifier: "Mapa_PaginaWeb_Segue", sender: self)
        
    }
    //ℹ️ - Que hacer

    //📌🙋🚂 - Seguir al usuario en el mapa - tracking (punto azul)
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print("Entra a: didChangeAuthorization")
        mapa.showsUserLocation = (status == .authorizedAlways)
        
    }
    //📌🙋🚂 - Seguir al usuario en el mapa - tracking (punto azul)
    
    //📌🙋🚂 - Actualiza
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("didUpdate")

        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        let region = MKCoordinateRegion(center: locValue, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))

        print(locationManager.monitoredRegions)
        
        //Guardamos nuestra curren location
        var array = locations
        myCurrentLocation_ = array.popLast()!
        
        //Conseguimos el lugar mas cercano
        self.getNearPlaceTo(myCurrentLocation: myCurrentLocation_, geofences: arrayGeo)
        
        //Actualizamos el mapa en donde nos dirigimos
        //self.mapa.setRegion(region, animated: true)

    }
    //📌🙋🚂 - Actualiza
    
    //📌🙋🚂 - Error en el perimetro
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        print("Monitoring failed for region with identifier: \(region!.identifier)")
        
    }
    //📌🙋🚂 - Error en el perimetro
    
    //📌🙋🚂 - Manejo de errorers
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location Manager failed with the following error: \(error)")
        
    }
    //📌🙋🚂 - Manejo de errorers
    
    //📌🙋🚂 - Como en background nos desja checar hasta 20 lugares, obtenemos los mas cercanos
    func getNearPlaceTo(myCurrentLocation: CLLocation, geofences:[Geotification]){
        var sortedGeo = [Geotification]()
        
        if geofences.count > 0 {
            for geofence in geofences{
                let location = CLLocation(latitude: geofence.lat, longitude: geofence.lng)
                let distance = myCurrentLocation.distance(from: location)
                geofence.distance = distance
                sortedGeo.append(geofence)
                
            }
            //Ordenamos los objetos por su propiedad .distance
            sortedGeo = sortedGeo.sorted(by: {$0.distance < $1.distance})
            //Limpiamos overlays antiguos
            self.mapa.removeOverlays(self.mapa.overlays)
            //Agregamos overlay al lugar(s) mas cercano(s)
            
            addRadiusOverlay(forGeotification: sortedGeo[0])
            addRadiusOverlay(forGeotification: sortedGeo[1])

            self.startMonitoring(geotification: sortedGeo[0])
            self.startMonitoring(geotification: sortedGeo[1])
            
        }
        
    }
    //📌🙋🚂 - Como en background nos desja checar hasta 20 lugares, obtenemos los mas cercanos
    
    //📌🙋🚂 - Agregamos un radio visual al marcador
    func addRadiusOverlay(forGeotification geotification: Geotification) {
        let lat = geotification.lat
        let lng = geotification.lng
        let coordinate = CLLocationCoordinate2D(latitude: lat!, longitude: lng!)
        let radius = CLLocationDistance(Double(geotification.radio))
        
        mapa?.add(MKCircle(center: coordinate, radius: radius))
        
    }
    //📌🙋🚂 - Agregamos un radio al marcador
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKCircle {
            let circleRenderer = MKCircleRenderer(overlay: overlay)
            circleRenderer.lineWidth = 1.0
            circleRenderer.strokeColor = .purple
            circleRenderer.fillColor = UIColor.purple.withAlphaComponent(0.4)
            return circleRenderer
        }
        return MKOverlayRenderer(overlay: overlay)
        
    }
    //📌🙋🚂 - Agregamos un radio al marcador
    
    //📌🙋🚂 - Agregamos un radio logico al marcador
    func region(withGeotification geotification: Geotification) -> CLCircularRegion {
        let coordinate = CLLocationCoordinate2D(latitude: geotification.lat, longitude: geotification.lng)
        let radius = CLLocationDistance(geotification.radio)
        // 1
        let region = CLCircularRegion(center: coordinate, radius: radius, identifier: geotification.id)
        // 2
        region.notifyOnEntry = true
        
        return region
    }
    //📌🙋🚂 - Agregamos un radio al marcador
    
    //📌🙋🚂 - Agregamos un radio al marcador
    func startMonitoring(geotification: Geotification) {
        // 1
        if !CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
            print("Geofencing is not supported on this device!")
            return
        }
        // 2
        if CLLocationManager.authorizationStatus() != .authorizedAlways {
            print("Your geotification is saved but will only be activated once you grant Geotify permission to access the device location.")
        }
        // 3
        let region = self.region(withGeotification: geotification)
        // 4
        locationManager.startMonitoring(for: region)
        
        print(locationManager.monitoredRegions)
    }
    //📌🙋🚂 - Agregamos un radio al marcador
    
    @IBAction func updateCurrentLocation(_ sender: UIBarButtonItem) {
        mapa.zoomToUserLocation()
        
    }
    
    @IBAction func unwindToMap(segue: UIStoryboardSegue) {}
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Mapa_Segue_InfoMapa"{
            let infmapvc = segue.destination as! InfoMapaViewController
            
            infmapvc.pntInteInf.id = geotifi.id
            infmapvc.pntInteInf.img = geotifi.img
            infmapvc.pntInteInf.name = geotifi.name
            infmapvc.pntInteInf.des = geotifi.des
            infmapvc.pntInteInf.subname = geotifi.subname
            infmapvc.pntInteInf.audio = geotifi.audio
            
        }
        if segue.identifier == "Mapa_PaginaWeb_Segue" {
            let pagwebvc = segue.destination as! PaginaWebMapaViewController
            pagwebvc.botonSleccionado = self.botonSeleccionado
            
        }
        
    }
    
    
    @IBAction func changeMap(_ sender: Any) {
        if self.mapa.mapType == MKMapType.standard {
            self.mapa.mapType = MKMapType.satellite
        } else {
            self.mapa.mapType = MKMapType.standard
        }
    }
    
    

}
