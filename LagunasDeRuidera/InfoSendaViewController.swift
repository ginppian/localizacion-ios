//
//  InfoSendaViewController.swift
//  LagunasDeRuidera
//
//  Created by ginppian on 03/09/16.
//  Copyright © 2016 ginppian. All rights reserved.
//

import UIKit
import AVFoundation



class InfoSendaViewController: UIViewController, UITabBarControllerDelegate {
    
    //Outlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelSubname: UILabel!
    @IBOutlet weak var desc: UITextView!
    
    //🎵
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var imageButonPlayPause: UIImageView!
    @IBOutlet weak var butonPlayPause: UIButton!
    @IBOutlet weak var labelTimer: UILabel!
    //🎵
    
    //Propertys
    var sendaInfo = Sendas()
    
    //🎵
    var path: String!
    var url: NSURL!
    var audioGuia = AVAudioPlayer()
    //🎵
    
    //⏰⏲⏰
    var timer = Timer()
    //⏰⏲⏰
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let imag = UIImage(named: sendaInfo.img)
        
        if imag != nil{
            self.imageView.image = imag
        } else {
            self.imageView.image = UIImage(named: "tobaantigualengua.jpg")
        }
        
        self.labelName.text = sendaInfo.name
        self.labelSubname.text = "\(sendaInfo.duration), \(sendaInfo.longitude)"
        self.desc.text = sendaInfo.des
        
        //🎵
        //🎧🎹🎷🎺
        self.path = Bundle.main.path(forResource: sendaInfo.audio, ofType: ".mp3")!
        //🎧🎹🎷🎺
        self.url = NSURL(fileURLWithPath: self.path)
        
        do {
            try self.audioGuia = AVAudioPlayer(contentsOf: self.url as URL)
        } catch {
            print("error")
        }
        
        self.slider.maximumValue = Float(self.audioGuia.duration)
        self.audioGuia.play()
        //🎵

        
        //🎵
        self.tabBarController?.delegate = self
        //🎵
        
        //⏰⏲⏰
        timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(InfoMapaViewController.updateSlider), userInfo: nil, repeats: true)
        //⏰⏲⏰
    }
    
    //🎵
    @IBAction func sliderAction(sender: UISlider) {
        self.audioGuia.stop()
        self.audioGuia.currentTime = TimeInterval(self.slider.value)
        
        //⏰⏲⏰
        self.labelTimer.text = self.secondsToHoursMinutesSeconds(seconds: TimeInterval(self.slider.value))
        //⏰⏲⏰
        
        self.audioGuia.prepareToPlay()
        self.audioGuia.play()
        self.imageButonPlayPause.image = UIImage(named: "pause.png")

    }
    
    @IBAction func butonPlayPauseAction(sender: UIButton) {
        if self.audioGuia.isPlaying == true {
            self.audioGuia.pause()
            self.imageButonPlayPause.image = UIImage(named: "play.png")
        } else {
            self.audioGuia.play()
            self.imageButonPlayPause.image = UIImage(named: "pause.png")
        }
    }
    //🎵
    
    //⏰⏲⏰
    func secondsToHoursMinutesSeconds (seconds : Double) -> String {
        let (_,  minf) = modf (seconds / 3600)
        let (min, secf) = modf (60 * minf)
        
        let min2 = self.roundToPlaces(value: min, places: 1)
        let secf2 = secf*60
        let secf3 = self.roundToPlaces(value: secf2, places: 0)
        
        return "\(Int(min2)): \(Int(secf3))"
    }
    //⏰⏲⏰
    
    //⏰⏲⏰
    func roundToPlaces(value:Double, places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return round(value * divisor) / divisor
    }
    //⏰⏲⏰
    
    //🎵
    internal func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController){
        print("Cambio de View el TabBar")
        if(self.audioGuia.isPlaying){
            self.audioGuia.stop()
            self.imageButonPlayPause.image = UIImage(named: "play.png")
        }
    }
    //🎵
    
    //⏰⏲⏰
    func updateSlider(){
        slider.value = Float(self.audioGuia.currentTime)
        self.labelTimer.text = self.secondsToHoursMinutesSeconds(seconds: TimeInterval(self.slider.value))
    }
    //⏰⏲⏰
    
    
    //🎵
    override func willMove(toParentViewController parent: UIViewController?) {
        super.willMove(toParentViewController: parent)
        if parent == nil {
            print("This VC is 'will' be popped. i.e. the back button was pressed.")
            self.audioGuia.stop()
        }
    }
    //🎵
    
    
    @IBAction func backToMap(_ sender: Any) {
        //tabBarController!.selectedIndex = 0
        performSegue(withIdentifier: "MapaConSenda", sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "MapaConSenda"{
            // hacemos aquí lo que haga falta
            
            let infosendvc = segue.destination as! SendaMapaViewController
            infosendvc.sendaInfo = self.sendaInfo
        }
    }
    
    
    
}
