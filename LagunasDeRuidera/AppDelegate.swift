//
//  AppDelegate.swift
//  pruebaGeotification
//
//  Created by ginppian on 24/10/16.
//  Copyright © 2016 ginppian. All rights reserved.
//

import UIKit
import CoreLocation
import AudioToolbox

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let locationManager = CLLocationManager()


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        
        UINavigationBar.appearance().barTintColor = UIColor(red: 103/255, green: 141/255, blue: 240/255, alpha: 0.8)
        UINavigationBar.appearance().tintColor = UIColor.white
        //Cambiar el color de los iconos de TAB BAR
        UITabBar.appearance().tintColor = UIColor.black
        
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        application.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
        UIApplication.shared.cancelAllLocalNotifications()
        return true
    }
    
    func handleEvent(forRegion region: CLRegion!) {
        // Mostramos una alerta
        if UIApplication.shared.applicationState == .active {
            let message = region.identifier
            print("message: \(message)")
            //window?.rootViewController?.showAlert(withTitle: nil, message: message)
            self.window?.rootViewController?.presentedViewController?.showAlert(withTitle: "Lugar Cerca 📍", message: message)
        } else {
            // De otra manera hacemos que vibre
            AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
            // Y presentamos una local notification
            let notification = UILocalNotification()
            notification.alertBody = region.identifier
            notification.soundName = "Default"
            UIApplication.shared.presentLocalNotificationNow(notification)
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    

}
extension AppDelegate: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        print("didEnter")
        if region is CLCircularRegion {
            handleEvent(forRegion: region)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        print("didExit")
        if region is CLCircularRegion {
            handleEvent(forRegion: region)
        }
    }
}
extension UIViewController {
//    func showAlert(withTitle title: String?, message: String?) {
//        print("Alert")
//        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
//        let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
//        alert.addAction(action)
//        present(alert, animated: true, completion: nil)
//    }
    func showAlert(withTitle title: String?, message: String?) {
        print("Alert")
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}

