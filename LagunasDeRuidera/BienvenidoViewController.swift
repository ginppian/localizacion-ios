//
//  BienvenidoViewController.swift
//  LagunasDeRuidera
//
//  Created by David Santiago on 27/11/16.
//  Copyright © 2016 ginppian. All rights reserved.
//

import UIKit
import AVFoundation

class BienvenidoViewController: UIViewController {
    
    var path: String!
    var url: NSURL!
    var audioGuia = AVAudioPlayer()
    
    let defaults = UserDefaults.standard
    var bandera = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Activa viewDidAppear
        super.viewDidAppear(true)
        
        var file = "audio37";
        
        self.bandera = self.defaults.integer(forKey: "idioma")
        if (self.bandera == 1) {
            file = "audio37_en"
        }
        
        self.path = Bundle.main.path(forResource: file, ofType: ".mp3")!
        self.url = NSURL(fileURLWithPath: self.path)
        
        do {
            try self.audioGuia = AVAudioPlayer(contentsOf: self.url as URL)
        } catch {
            print("error")
        }
        
        self.audioGuia.play()
    }
    
    
    @IBAction func GoToMap(_ sender: Any) {
        self.audioGuia.stop()
        performSegue(withIdentifier: "Map_Segue", sender: self)
    }
}
