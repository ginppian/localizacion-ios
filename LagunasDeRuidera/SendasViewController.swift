//
//  SendasViewController.swift
//  LagunasDeRuidera
//
//  Created by ginppian on 27/08/16.
//  Copyright © 2016 ginppian. All rights reserved.
//

//📲 #Hace REFERENCIA a la funcion SWIPE

import UIKit

class SendasViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    //Outlets
    @IBOutlet weak var tableView: UITableView!
    
    //Arrays
    var arrayImg = [String]()
    var arrayIdText = [String]()
    var arrayNumPoints = [String]()
    var arrayNam = [String]()
    var arrayDes = [String]()
    var arrayLon = [String]()
    var arrayDur = [String]()
    //🎧🎹🎷🎺
    var arrayAud = [String]()
    //🎧🎹🎷🎺
    
    //Propertys
    //📲
    var swipeRight = UISwipeGestureRecognizer()
    //📲
    var sendaID: Int!
    
    //🇺🇸🇲🇽🇪🇸
    let defaults = UserDefaults.standard
    var bandera = Int()
    var idiomaArchivo = String()
    //espanol = 0
    //defaults.integerForKey("espanol")
    //ingles = 1
    //defaults.integerForKey("ingles")
    //🇺🇸🇲🇽🇪🇸
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        //🇺🇸🇲🇽🇪🇸
        self.bandera = self.defaults.integer(forKey: "idioma")
        if(self.bandera == 0){
            self.idiomaArchivo = "routes"
        }
        else if (self.bandera == 1) {
            self.idiomaArchivo = "routes_en"
        } else {
            print("Error al asignar la bandera de idioma")
        }
        //🇺🇸🇲🇽🇪🇸

        //📲
        swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(SendasViewController.transicionDerAIzq))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        //📲
        
        
        self.automaticallyAdjustsScrollViewInsets = false// Esta linea de codigo se pone porque xcode mueve nuestro UICollectionViewCell
        
        arrayIdText = JSONReader.readJSONToArrayString(fileName: idiomaArchivo, type: ".json", key: "id")
        arrayNumPoints = JSONReader.readJSONToArrayString(fileName: idiomaArchivo, type: ".json", key: "numPoints")
        arrayImg = JSONReader.readJSONToArrayString(fileName: idiomaArchivo, type: ".json", key: "img")
        arrayNam = JSONReader.readJSONToArrayString(fileName: idiomaArchivo, type: ".json", key: "name")
        arrayDes = JSONReader.readJSONToArrayString(fileName: idiomaArchivo, type: ".json", key: "description")
        arrayLon = JSONReader.readJSONToArrayString(fileName: idiomaArchivo, type: ".json", key: "longitude")
        arrayDur = JSONReader.readJSONToArrayString(fileName: idiomaArchivo, type: ".json", key: "duration")
        //🎧🎹🎷🎺
        arrayAud = JSONReader.readJSONToArrayString(fileName: idiomaArchivo, type: ".json", key: "audio")
        //🎧🎹🎷🎺
        
        let topNB = UIImage(named: "cabecera_mapa_azul.png")
        let topNBR = resizeImageNav(image: topNB!, newWidth: 250)
        let topNBView = UIImageView(image: topNBR)
        self.navigationController?.navigationBar.topItem?.titleView = topNBView
        self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    func resizeImageNav(image: UIImage, newWidth: CGFloat) -> UIImage
    {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(newWidth, newHeight))
        image.draw(in: CGRect(0, 0, newWidth, newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }

    //📲
    func transicionDerAIzq(){
        
        //Reducimos el index del tabBar
        self.tabBarController?.selectedIndex -= 1
        
        let slideInFromLeftTransition = CATransition()
        slideInFromLeftTransition.type = kCATransitionPush
        slideInFromLeftTransition.subtype = kCATransitionFromLeft
        slideInFromLeftTransition.duration = 0.8
        slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        slideInFromLeftTransition.fillMode = kCAFillModeRemoved
        self.tabBarController?.view.layer.add(slideInFromLeftTransition, forKey: "slideInFromLeftTransition")
    }
    //📲

    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.arrayNam.count
    
    }
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "sendasCell", for:  indexPath as IndexPath) as! SendasTableViewCell
        
        let imgNam = arrayImg[indexPath.row] + ".jpg"
        let imag = UIImage(named: imgNam)
        if imag != nil{
            cell.imagen!.image = imag
        } else {
            cell.imagen.image = UIImage(named: "tobaantigualengua.jpg")
        }
        
        cell.labelTitulo.text = arrayNam[indexPath.row]
        cell.labelLongitud.text = "Longitud: \(arrayLon[indexPath.row])"
        cell.labelDuracion.text = "Duración: \(arrayDur[indexPath.row])"
        
        return cell
    
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        sendaID = indexPath.row
        print("Sleccionaste la celda: " + "\(indexPath.row)")

        performSegue(withIdentifier: "Senda_InfoMapa_Segue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Senda_InfoMapa_Segue"{
            let infosendvc = segue.destination as! InfoSendaViewController
            infosendvc.sendaInfo.id = sendaID
            infosendvc.sendaInfo.idtext = arrayIdText[sendaID]
            infosendvc.sendaInfo.numPoints = arrayNumPoints[sendaID]
            infosendvc.sendaInfo.img = arrayImg[sendaID] + ".jpg"
            infosendvc.sendaInfo.name = arrayNam[sendaID]
            infosendvc.sendaInfo.des = arrayDes[sendaID]
            infosendvc.sendaInfo.duration = arrayDur[sendaID]
            infosendvc.sendaInfo.longitude = arrayLon[sendaID]
            //🎧🎹🎷🎺
            infosendvc.sendaInfo.audio = arrayAud[sendaID]
            //🎧🎹🎷🎺
        }
    }

}
