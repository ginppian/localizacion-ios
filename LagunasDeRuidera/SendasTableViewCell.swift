//
//  SendasTableViewCell.swift
//  LagunasDeRuidera
//
//  Created by ginppian on 28/08/16.
//  Copyright © 2016 ginppian. All rights reserved.
//

import UIKit

class SendasTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imagen: UIImageView!
    
    @IBOutlet weak var labelTitulo: UILabel!
    @IBOutlet weak var labelLongitud: UILabel!
    @IBOutlet weak var labelDuracion: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor(red: 252/255, green: 155/255, blue: 44/255, alpha: 1.0)
        
        bgColorView.layer.cornerRadius = 8
        //selectedBackgroundView = bgColorView
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }


    
    
}
