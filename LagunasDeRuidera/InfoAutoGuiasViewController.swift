//
//  InfoAutoGuiasViewController.swift
//  LagunasDeRuidera
//
//  Created by ginppian on 03/09/16.
//  Copyright © 2016 ginppian. All rights reserved.
//

import UIKit
import AVFoundation

class InfoAutoGuiasViewController: UIViewController, UITabBarControllerDelegate {

    //Outlets
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    //🎵
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var butonPlayPause: UIButton!
    @IBOutlet weak var imageButonPlayPause: UIImageView!
    @IBOutlet weak var labelTimer: UILabel!
    //🎵
    
    //Propertys
    var audioInfo = Audios()
    var infoID = 0
    
    //🎵
    var path: String!
    var url: NSURL!
    var audioGuia = AVAudioPlayer()
    //🎵
    
    //⏰⏲⏰
    var timer: Timer!
    //⏰⏲⏰
    
    //🇺🇸🇲🇽🇪🇸
    let defaults = UserDefaults.standard
    var bandera = Int()
    var idiomaArchivo = String()
    //espanol = 0
    //defaults.integerForKey("espanol")
    //ingles = 1
    //defaults.integerForKey("ingles")
    //🇺🇸🇲🇽🇪🇸
    
    //Arrays
    var arrayName = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //🇺🇸🇲🇽🇪🇸
        self.bandera = self.defaults.integer(forKey: "idioma")
        if(self.bandera == 0){
            self.idiomaArchivo = "audio"
        }
        else if (self.bandera == 1) {
            self.idiomaArchivo = "audio_en"
        } else {
            print("Error al asignar la bandera de idioma")
        }
        //🇺🇸🇲🇽🇪🇸
        
        //🇺🇸🇲🇽🇪🇸
        arrayName = JSONReader.readJSONToArrayString(fileName: idiomaArchivo, type: ".json", key: "name")
        
        self.textView.text = self.audioInfo.text
        //Margenes - Sangria
        self.textView.textContainer.lineFragmentPadding = 13;
        
        infoID = Int(self.audioInfo.id)
        labelName.text = arrayName[infoID]
        
        print("InfoAutoGuias, ID: \(audioInfo.id)")
        
        
        //🎵
        //🎧🎹🎷🎺
        self.path = Bundle.main.path(forResource: self.audioInfo.audio, ofType: ".mp3")!
        //🎧🎹🎷🎺
        self.url = NSURL(fileURLWithPath: self.path)
        
        do {
            try self.audioGuia = AVAudioPlayer(contentsOf: self.url as URL)
        } catch {
            print("error")
        }
        
        self.slider.maximumValue = Float(self.audioGuia.duration)
        self.audioGuia.play()
        //🎵
        
        //🎵
        self.tabBarController?.delegate = self
        //🎵
        
        //⏰⏲⏰
        timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(InfoMapaViewController.updateSlider), userInfo: nil, repeats: true);
        //⏰⏲⏰
        
    }
    //Esta linea se pone porque el texto en el scrollview aparece en medio o abajo por defecto
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        textView.setContentOffset(CGPoint.zero, animated: false)
    }
    
    //🎵
    @IBAction func sliderAction(sender: UISlider) {
        self.audioGuia.stop()
        self.audioGuia.currentTime = TimeInterval(self.slider.value)
        
        //⏰⏲⏰
        self.labelTimer.text = self.secondsToHoursMinutesSeconds(seconds: TimeInterval(self.slider.value))
        //⏰⏲⏰
        
        self.audioGuia.prepareToPlay()
        self.audioGuia.play()
        self.imageButonPlayPause.image = UIImage(named: "pause.png")

    }
    //🎵
    
    //🎵
    @IBAction func butonPlayPauseAction(sender: UIButton) {
        if self.audioGuia.isPlaying == true {
            self.audioGuia.pause()
            self.imageButonPlayPause.image = UIImage(named: "play.png")
        } else {
            self.audioGuia.play()
            self.imageButonPlayPause.image = UIImage(named: "pause.png")
        }
    }
    //🎵
    
    //⏰⏲⏰
    func secondsToHoursMinutesSeconds (seconds : Double) -> String {
        let (_,  minf) = modf (seconds / 3600)
        let (min, secf) = modf (60 * minf)
        
        let min2 = self.roundToPlaces(value: min, places: 1)
        let secf2 = secf*60
        let secf3 = self.roundToPlaces(value: secf2, places: 0)
        
        return "\(Int(min2)): \(Int(secf3))"
    }
    //⏰⏲⏰
    
    //⏰⏲⏰
    func roundToPlaces(value:Double, places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return round(value * divisor) / divisor
    }
    //⏰⏲⏰
    
    //🎵
    internal func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController){
        print("Cambio de View el TabBar")
        if(self.audioGuia.isPlaying){
            self.audioGuia.stop()
            self.imageButonPlayPause.image = UIImage(named: "play.png")
        }
    }
    //🎵
    
    //⏰⏲⏰
    func updateSlider(){
        slider.value = Float(self.audioGuia.currentTime)
        self.labelTimer.text = self.secondsToHoursMinutesSeconds(seconds: TimeInterval(self.slider.value))
    }
    //⏰⏲⏰
    
    
    //🎵
    override func willMove(toParentViewController parent: UIViewController?) {
        super.willMove(toParentViewController: parent)
        if parent == nil {
            print("This VC is 'will' be popped. i.e. the back button was pressed.")
            self.audioGuia.stop()
        }
    }
    //🎵
}
