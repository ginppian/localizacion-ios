//
//  AutoguiasViewController.swift
//  LagunasDeRuidera
//
//  Created by ginppian on 27/08/16.
//  Copyright © 2016 ginppian. All rights reserved.
//

//📲 #Hace REFERENCIA a la funcion SWIPE


import UIKit

class AutoguiasViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    //Outlets
    @IBOutlet weak var tableView: UITableView!
    
    //Arrays
    var arrayAudioGuias = [String]()
    var arrayID = [String]()
    var arrayFile = [String]()
    //🎧🎹🎷🎺 CABE ACLARAR QUE EL ID DEL ARCHIVO ADUI.JSON ES EL NOMBRE DEL AUDIO, POR ESO MANEJAMOS EL AUDIO CON EL ID
    // ATENCIÓN: Rectificación David: El nombre del audio no es el ID, es la propiedad file
    
    //ℹ️
    var arrayText = [String]()
    //ℹ️
    
    //Propertys
    //📲
    var swipeLeft = UISwipeGestureRecognizer()
    var swipeRight = UISwipeGestureRecognizer()
    //📲
    
    //🇺🇸🇲🇽🇪🇸
    let defaults = UserDefaults.standard
    var bandera = Int()
    var idiomaArchivo = String()
    //espanol = 0
    //defaults.integerForKey("espanol")
    //ingles = 1
    //defaults.integerForKey("ingles")
    //🇺🇸🇲🇽🇪🇸
    
    var audioID: Int!
    var bgColorView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //🇺🇸🇲🇽🇪🇸
        self.bandera = self.defaults.integer(forKey: "idioma")
        if(self.bandera == 0){
            self.idiomaArchivo = "audio"
        }
        else if (self.bandera == 1) {
            self.idiomaArchivo = "audio_en"
        } else {
            print("Error al asignar la bandera de idioma")
        }
        //🇺🇸🇲🇽🇪🇸
        
        //📲
        swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(AutoguiasViewController.transicionIzqADerecha))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)
        
        swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(AutoguiasViewController.transicionDerAIzq))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        //📲
        
        //self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.automaticallyAdjustsScrollViewInsets = false// Esta linea de codigo se pone porque xcode mueve nuestro UICollectionViewCell
        
        //🇺🇸🇲🇽🇪🇸
        arrayAudioGuias = JSONReader.readJSONToArrayString(fileName: idiomaArchivo, type: ".json", key: "name")
        
        //🎧🎹🎷🎺
        arrayID = JSONReader.readJSONToArrayString(fileName: idiomaArchivo, type: ".json", key: "id")
        arrayFile = JSONReader.readJSONToArrayString(fileName: idiomaArchivo, type: ".json", key: "file")
        //🎧🎹🎷🎺
        
        bgColorView.backgroundColor = UIColor(red: 252/255, green: 155/255, blue: 44/255, alpha: 1.0)
        bgColorView.layer.cornerRadius = 5
        
        //ℹ️
        arrayText = JSONReader.readJSONToArrayString(fileName: idiomaArchivo, type: ".json", key: "text")
        //ℹ️
        
        //🇺🇸🇲🇽🇪🇸
        
        //
        let topNB = UIImage(named: "cabecera_mapa_azul.png")
        let topNBR = resizeImageNav(image: topNB!, newWidth: 250)
        let topNBView = UIImageView(image: topNBR)
        self.navigationController?.navigationBar.topItem?.titleView = topNBView
        self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.navigationBar.isTranslucent = false

    }
    
    func resizeImageNav(image: UIImage, newWidth: CGFloat) -> UIImage
    {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(newWidth, newHeight))
        image.draw(in: CGRect(0, 0, newWidth, newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }

    //📲
    func transicionIzqADerecha(){
        
        //Aumenamos el index del tabBar
        self.tabBarController?.selectedIndex += 1

        let slideInFromLeftTransition = CATransition()
        slideInFromLeftTransition.type = kCATransitionPush
        slideInFromLeftTransition.subtype = kCATransitionFromRight
        slideInFromLeftTransition.duration = 0.8
        slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        slideInFromLeftTransition.fillMode = kCAFillModeRemoved
        self.tabBarController?.view.layer.add(slideInFromLeftTransition, forKey: "slideInFromLeftTransition")
    }
    //📲
    
    //📲
    func transicionDerAIzq(){
        
        //Reducimos el index del tabBar
        self.tabBarController?.selectedIndex -= 1
        
        let slideInFromLeftTransition = CATransition()
        slideInFromLeftTransition.type = kCATransitionPush
        slideInFromLeftTransition.subtype = kCATransitionFromLeft
        slideInFromLeftTransition.duration = 0.8
        slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        slideInFromLeftTransition.fillMode = kCAFillModeRemoved
        self.tabBarController?.view.layer.add(slideInFromLeftTransition, forKey: "slideInFromLeftTransition")
    }
    //📲
    
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.arrayAudioGuias.count
    }
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell:UITableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "cellAudioGuias")! as UITableViewCell
        
        cell.textLabel?.text = self.arrayAudioGuias[indexPath.row]
        

        cell.selectedBackgroundView = bgColorView
        
        return cell
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        print("Sleccionaste la celda: \(indexPath.row)")
        self.audioID = indexPath.row

        performSegue(withIdentifier: "Autoguias_InfoAutoguias_Segue", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "Autoguias_InfoAutoguias_Segue"{
            let infaudiogvc = segue.destination as! InfoAutoGuiasViewController
            
            infaudiogvc.audioInfo.text = arrayText[self.audioID]
            infaudiogvc.audioInfo.id = self.audioID
            
            //🎧🎹🎷🎺
            //infaudiogvc.audioInfo.audio = arrayID[self.audioID]
            infaudiogvc.audioInfo.audio = arrayFile[self.audioID]
            
            
        }
    }


}
