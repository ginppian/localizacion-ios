//
//  PaginaWebMapaViewController.swift
//  LagunasDeRuidera
//
//  Created by ginppian on 06/09/16.
//  Copyright © 2016 ginppian. All rights reserved.
//

import UIKit

class PaginaWebMapaViewController: UIViewController {
    
    let defaults = UserDefaults.standard
    var bandera = Int()

    var botonSleccionado = String()
    @IBOutlet weak var webView: UIWebView!
    
    var url: NSURL!
    var requestObj: NSURLRequest!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.bandera = self.defaults.integer(forKey: "idioma")
        
        if(botonSleccionado == "informacion"){
        
            url = NSURL (string: "http://www.laslagunasderuidera.org/informacion")
            
            if (self.bandera == 1) {
                url = NSURL (string: "http://www.laslagunasderuidera.org/information")
            }
            
            requestObj = NSURLRequest(url: url! as URL);
            webView.loadRequest(requestObj as URLRequest);
        }
        if(botonSleccionado == "normativa"){
            
            url = NSURL (string: "http://www.laslagunasderuidera.org/el-parque-natural")
            
            if (self.bandera == 1) {
                url = NSURL (string: "http://www.laslagunasderuidera.org/naturalpark")
            }
            
            requestObj = NSURLRequest(url: url! as URL);
            webView.loadRequest(requestObj as URLRequest);
        
        }
        if(botonSleccionado == "queHacer"){
            
            url = NSURL (string: "http://www.laslagunasderuidera.org/donquijote")
            
            if (self.bandera == 1) {
                url = NSURL (string: "http://www.laslagunasderuidera.org/donquixote")
            }

            requestObj = NSURLRequest(url: url! as URL);
            webView.loadRequest(requestObj as URLRequest);
        
        }

    
    
    }


}
